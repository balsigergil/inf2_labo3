/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Joueur.cpp
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Implémentation de la classe Joueur.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#include "Outil.h"         // utilisation du générateur aléatoire 
#include "Constantes.h"    // règles du jeu 
#include <algorithm>       // max element, sort 
#include <iostream>        // cout
#include "Joueur.h"        // utilisation de la classe Joueur

using namespace std;

Joueur::Joueur(const std::string& nom, bool estIntelligent, const vector<Carte>& cartes)
        : nom(nom), cartesEnMain(cartes), points(0), estIntelligent(estIntelligent) {}

void Joueur::jouer(Joueur* joueurAttaquer) {
    bool carteValide;
    // Tant que les cartes demandées sont juste
    do {
        detecterFamille();
        Carte carteDemander = carteADemander();
        // Demander carte à joueur attaqué
        carteValide = joueurAttaquer->demanderCarte(carteDemander);

        cout << nom << " demande a " << joueurAttaquer->nom << " la carte " << carteDemander.getFamille()
             << carteDemander.getMembre();
        // Affichage du résultat de la demande de carte à l'autre joueur
        if (carteValide) {
            cout << " et " << joueurAttaquer->nom << " donne la carte a " << nom << endl;
        } else {
            cout << " mais " << joueurAttaquer->nom << " ne l'a pas" << endl;
        }
        // Si la carte demandée est juste, l'ajouter à la main
        if (carteValide) {
            cartesEnMain.push_back(carteDemander);
        }
    } while (carteValide && estEnJeu());
}

bool Joueur::demanderCarte(const Carte& carte) {
    // Trouver la carte dans la main du joueur attaqué
    auto it = find(cartesEnMain.begin(), cartesEnMain.end(), carte);
    if (it != cartesEnMain.end()) {
        // Si le joueur attaqué à la carte demandé, la supprimer de sa main
        cartesEnMain.erase(it);
        return true;
    }
    return false;
}

Carte Joueur::carteADemander() {
    unsigned familleADemander;
    // Sélection du comportement à adopter en fonction de "l'intelligence" du joueur
    if (estIntelligent) {
        familleADemander = familleLaPlusNombreuse();
    } else if (!cartesEnMain.empty()) {
        // Si le joueur n'est pas "intelligent", la famille de la carte à demander et sélectionnée aléatoirement
        familleADemander = cartesEnMain.at(
                (unsigned) genererNombreAleatoire((int) cartesEnMain.size() - 1)).getFamille();
    } else {
        familleADemander = PREMIER_FAMILLE_CARTE;
    }

    // Création d'un vecteur avec tous les membres possédé de la famille à demander
    vector<char> membre;
    for (Carte& carte : cartesEnMain) {
        if (carte.getFamille() == familleADemander) {
            membre.push_back(carte.getMembre());
        }
    }

    // Création d'un vecteur avec tous les membres possibles
    vector<char> membreOppose(CARTES_PAR_FAMILLE);
    for (unsigned i = 0; i < membreOppose.size(); ++i) {
        membreOppose.at(i) = (char)(PREMIER_MEMBRE_CARTE + i);
    }

    // Suppression des membres en main du vecteur de membres possibles
    for (char j : membre) {
        remove(membreOppose.begin(), membreOppose.end(), j);
    }
    membreOppose.erase(membreOppose.end() - (int) membre.size(), membreOppose.end());

    // Sélection aléatoire d'une carte dans les des membres non possédé de la famille à demander
    return {membreOppose.at((unsigned) genererNombreAleatoire((int) membreOppose.size() - 1)), familleADemander};
}

unsigned Joueur::familleLaPlusNombreuse() {
    vector<int> nbCarteParFamille = compterCarteParFamille();
    return (PREMIER_FAMILLE_CARTE + distance(nbCarteParFamille.begin(), max_element(nbCarteParFamille.begin(), nbCarteParFamille.end())));
}

std::vector<int> Joueur::compterCarteParFamille() {
    vector<int> nbCarteParFamille(NOMBRE_FAMILLES, 0);
    // Pour chaque familles
    for (unsigned i = 0; i < NOMBRE_FAMILLES; ++i) {
        // Compter le nombre de carte de cette famille dans la main du joueur
        for (const Carte& carte : cartesEnMain) {
            if (carte.getFamille() == i + PREMIER_FAMILLE_CARTE) {
                ++nbCarteParFamille.at(i);
            }
        }
    }
    return nbCarteParFamille;
}

bool Joueur::detecterFamille() {
    vector<int> nbCarteParFamille = compterCarteParFamille();
    // Pour chaque familles
    for (unsigned i = 0; i < NOMBRE_FAMILLES; ++i) {
        // Si la famille est complète
        if (nbCarteParFamille.at(i) >= CARTES_PAR_FAMILLE) {
            // Déplacer les cartes de la main du joueur à la table
            for (unsigned j = 0; j < cartesEnMain.size(); ++j) {
                if (cartesEnMain.at(j).getFamille() == i + PREMIER_FAMILLE_CARTE) {
                    famillesSurTable.push_back(*(cartesEnMain.begin() + (int) j));
                    cartesEnMain.erase(cartesEnMain.begin() + (int) j);
                    --j;
                }
            }
            ++points;
            return true;
        }
    }
    return false;
}

void Joueur::ajouterCarte(const Carte& c) {
    cartesEnMain.push_back(c);
}

void Joueur::ajouterCarte(const std::vector<Carte>& nouvelleCartes) {
    this->cartesEnMain.insert(this->cartesEnMain.end(), nouvelleCartes.begin(), nouvelleCartes.end());

}

bool Joueur::estEnJeu() {
    return !cartesEnMain.empty();
}

unsigned int Joueur::getPoints() const {
    return points;
}

bool operator==(const Joueur& lhs, const Joueur& rhs) {
    return lhs.nom == rhs.nom &&
           lhs.cartesEnMain == rhs.cartesEnMain &&
           lhs.points == rhs.points;
}

bool operator!=(const Joueur& lhs, const Joueur& rhs) {
    return !(rhs == lhs);
}

const string& Joueur::getNom() const {
    return nom;
}

std::ostream& operator<<(std::ostream& os, const Joueur& joueur) {
    os << joueur.nom << " : ";
    for (Carte c : joueur.cartesEnMain) {
        os << c.getFamille() << c.getMembre() << " ";
    }
    os << "[";
    for (Carte c : joueur.famillesSurTable) {
        os << c.getFamille() << c.getMembre();
        if (c != joueur.famillesSurTable.back())
            os << ", ";
    }
    os << "]";
    return os;
}

void Joueur::reinitialiser() {
    cartesEnMain.clear();
    famillesSurTable.clear();
}
