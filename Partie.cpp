/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Partie.cpp
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Implémentation de la classe Partie.

 Remarque(s) :

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#include "Partie.h"         // header
#include "Outil.h"          // du générateur aléatoire
#include "Constantes.h"     // règles du jeu
#include "Joueur.h"         // utilisation de la classe Joueur
#include <iostream>         // cout
#include <algorithm>        // shuffle
#include <random>           // aléatoire
#include <chrono>           // chronomètre

using namespace std;
using namespace std::chrono;

Partie::Partie(const vector<Joueur*>& joueurs) : joueurs(joueurs) {}

vector<int> Partie::jeu() {
    high_resolution_clock::time_point startChrono;
    high_resolution_clock::time_point stopChrono;
    int nTour = 1;          // compteur de partie

    this->initPioche();     // initialisation de la pioche 
    this->initJeuJoueur();  // initialise le jeu des joueurs

    startChrono = std::chrono::high_resolution_clock::now();
    cout << "Debut de la partie des " << NOMBRE_FAMILLES << " familles" << endl;

    // On mix l'ordre des joueurs à chaque début de partie
    unsigned seed = (unsigned) std::chrono::system_clock::now().time_since_epoch().count();
    shuffle(joueurs.begin(), joueurs.end(), std::default_random_engine(seed));

    while (joueursEncoreEnJeu()) {
        cout << "\n*** Tour " << nTour++ << " ***" << endl;

        // Affichage des cartes des tous les joueurs et de la pioche
        for (Joueur* j : joueurs) {
            cout << *j << endl;
        }
        cout << "Pioche : ";
        for (Carte c : pioche)
            cout << c.getFamille() << c.getMembre() << " ";

        cout << endl;

        // passer au joueur suivant
        for (auto& joueur : joueurs) {
            vector<Joueur*> autresJoueurs;
            // liste des joueurs en jeu qui on peut demander des cartes
            for (Joueur* j : joueurs) {
                if (j != joueur && j->estEnJeu()) {
                    autresJoueurs.push_back(j);
                }
            }
            // choix aléatoire du joueur à attaquer
            Joueur* joueurAttaquer = autresJoueurs.at(
                    (unsigned) genererNombreAleatoire((int) (autresJoueurs.size() - 1)));

            // le joueur joue
            joueur->jouer(joueurAttaquer);

            // si il n'y a plus de joueur en jeu on sort de la partie
            if (!joueursEncoreEnJeu())
                break;

            // piocher carte tant qu'il y a des cartes dans la pioche 
            // affichage de la carte piochée 
            if (!pioche.empty()) {
                Carte cartePiochee = this->piocherCarte();
                cout << joueur->getNom() << " prend une carte dans la pioche (" << cartePiochee << ")" << endl;
                joueur->ajouterCarte(cartePiochee);
            }

        }
    }

    // Affichage des cartes des tous les joueurs et de la pioche
    for (Joueur* j : joueurs) {
        cout << *j << endl;
    }

    // affichage de fin
    cout << "\nLa partie est finie !" << endl;
    cout << "Nombre de tours : " << nTour << endl;
    stopChrono = high_resolution_clock::now();
    cout << "Temps de la partie : " << duration_cast<milliseconds>(stopChrono - startChrono).count() << " ms" << endl;
    // reinitialisation des joueurs
    for (Joueur* j : joueurs) {
        j->reinitialiser();
    }
    return scoreJoueurs();
}

void Partie::initPioche() {
    for (unsigned famille = PREMIER_FAMILLE_CARTE; famille <= NOMBRE_FAMILLES; ++famille) {
        for (char membre = PREMIER_MEMBRE_CARTE; membre < (PREMIER_MEMBRE_CARTE + CARTES_PAR_FAMILLE); ++membre) {
            Carte C(membre, famille);
            pioche.push_back(C);    // insère chaque famille entièrement
        }
    }
    // mélange du tas de carte avec une seed en fonction du temps
    unsigned seed = (unsigned) std::chrono::system_clock::now().time_since_epoch().count();
    shuffle(pioche.begin(), pioche.end(), std::default_random_engine(seed));
}

vector<int> Partie::scoreJoueurs() {
    vector<int> score;
    for (Joueur* j : joueurs) {
        score.push_back((int) j->getPoints());
    }
    // retourne le score de tous les joueurs
    return score;
}

Carte Partie::piocherCarte() {
    // copie de carte sur dessus de la pioche et suppression
    Carte carteADonner = pioche.back();
    cout << "Carte piochee : " << pioche.back() << endl;
    pioche.pop_back();
    return carteADonner;
}

void Partie::initJeuJoueur() {
    std::vector<Carte> C;
    // ajoute des cartes aux joueurs pour le début du jeu
    for (auto& it : joueurs) {
        C.assign(pioche.end() - CARTE_PAR_JOUEUR, pioche.end());
        it->ajouterCarte(C);
        pioche.erase(pioche.end() - CARTE_PAR_JOUEUR, pioche.end());
    }
}

bool Partie::joueursEncoreEnJeu() const {
    // test si plus d'un joueur est en jeu
    size_t joueurEnJeu = 0;
    for (Joueur* j : joueurs) {
        if (j->estEnJeu())
            joueurEnJeu++;
    }
    return joueurEnJeu > 1;
}
