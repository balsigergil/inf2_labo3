#!/bin/bash

echo ""
echo "Netbeans project creation script"
echo ""

export TMPDIR='temp' DISTDIR='dist' LAB_ID='Labo 03'

mkdir -p $TMPDIR

ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0`

tail -n+$ARCHIVE $0 | tar xzv -C $TMPDIR

echo "Copying sources to temp dir"
cp *.h $TMPDIR
cp *.cpp $TMPDIR
cd $TMPDIR

echo "Creating Netbeans project"
item_h=$(ls *.h | sed 's/\(.*\)/<itemPath>\1<\\\/itemPath>/' | tr '\r\n' ' ')
item_cpp=$(ls *.cpp | sed 's/\(.*\)/<itemPath>\1<\\\/itemPath>/' | tr '\r\n' ' ')
conf_h=$(ls *.h | sed 's/\(.*\)/<item path="\1" ex="false" tool="3" flavor2="0"><\\\/item>/' | tr '\r\n' ' ')
conf_cpp=$(ls *.cpp | sed 's/\(.*\)/<item path="\1" ex="false" tool="1" flavor2="0"><\\\/item>/' | tr '\r\n' ' ')
sed -i "s/###ITEM_H###/${item_h}/g" nbproject/configurations.xml
sed -i "s/###ITEM_CPP###/${item_cpp}/g" nbproject/configurations.xml
sed -i "s/###CONF_H###/${conf_h}/g" nbproject/configurations.xml
sed -i "s/###CONF_CPP###/${conf_cpp}/g" nbproject/configurations.xml
sed -i "s/###LAB_ID###/${conf_cpp}/g" nbproject/project.xml

mkdir -p ../$DISTDIR

if [ $1 = "--no-zip" ]
then
    cp -r * ../$DISTDIR
else
    echo "Creating zip archive"
    zip -r ../$DISTDIR/netbeans.zip *
fi

cd ..
rm -rf $TMPDIR

exit 0

__ARCHIVE_BELOW__
