/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Joueur.h
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Classe permettant la gestion des joueurs dans une partie.

 Remarque(s) : Le paramètre estIntelligent permet de définir la stratégie du joueur pour
               séléctionner la carte à demander.
               Si true, le joueur va demander une carte de la famille la plus nombreuse dans son jeu.
               Si false, le joueur va demander une carte d'une famille au hasard dans son jeu.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#ifndef LABO_03_JOUEUR_H
#define LABO_03_JOUEUR_H

#include "Carte.h"      // utilisation classe Carte
#include <vector>       // utilisation de vector
#include <string>       // utilisation de string
#include <ostream>      // surcharge opérateur de flux

/**
 * @class Gestion des paramètres d'un joueur et de son comportement en jeu.
 * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
 * @date 15.03.2019
 */
class Joueur {
    /**
     * @brief Surcharge de l'operateur == pour la classe Joueur.
     * @param lhs Joueur à comparer.
     * @param rhs Joueur à comparer.
     * @return True si les deux joueurs sont égaux.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    friend bool operator==(const Joueur& lhs, const Joueur& rhs);

    /**
     * @brief Surcharge de l'operateur != pour la classe Joueur.
     * @param lhs Joueur à comparer.
     * @param rhs Joueur à comparer.
     * @return True si les deux joueurs sont différents.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    friend bool operator!=(const Joueur& lhs, const Joueur& rhs);

    friend std::ostream& operator<<(std::ostream& os, const Joueur& joueur);

public:
    /**
     * @brief Constructeur de la classe joueur.
     * @param nom string, Nom du joueur.
     * @param cartes vecteur de Carte.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    Joueur(const std::string& nom, bool estIntelligent = false, const std::vector<Carte>& cartes = {});

    /**
     * @brief Fait jouer un tour du joueur actuel.
     * @param joueurs vecteur de pointeur des joueurs en jeu.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    void jouer(Joueur* joueurAttaquer);

    /**
     * @brief Ajoute une carte au joueur actuel.
     * @param nouvelleCarte carte à ajouter.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    void ajouterCarte(const Carte& nouvelleCarte);

    /**
     * @brief Ajoute des cartes au joueur actuel.
     * @param nouvelleCartes vecteur de carte à ajouter.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    void ajouterCarte(const std::vector<Carte>& nouvelleCartes);

    /**
     * @brief Est-ce que le joueur actuel est encore en jeu.
     * @return True si le joueur actuel est en jeu.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    bool estEnJeu();

    /**
     * @brief Obtiens le nombre de points du joueur actuel.
     * @return Le nombre de points du joueur actuel.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    unsigned int getPoints() const;

    const std::string& getNom() const;

    /**
     * @brief Réinitialise les cartes du joueur (familles posées et cartes en main)
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    void reinitialiser();

private:
    /**
     * @brief Nom du joueur.
     */
    std::string nom;

    /**
     * @brief Liste des cartes que le joueur possède.
     */
    std::vector<Carte> cartesEnMain;

    /**
     * @brief Liste des cartes que le joueur a posé.
     */
    std::vector<Carte> famillesSurTable;

    /**
     * @brief Points attribués au joueur. 1 point est donné quand le joueur possède une famille complète.
     */
    unsigned int points;

    /**
     * @brief bool. Permet de définir la stratégie du joueur pour séléctionner la carte à demander.
     *        Si true, le joueur va demander une carte de la famille la plus nombreuse dans son jeu.
     *        Si false, le joueur va demander une carte d'une famille au hasard dans son jeu.
     */
    bool estIntelligent;

    /**
     * @brief Demande une carte au joueur membre. Si dernier a la carte demandée, elle est supprimé de ses cartes et ajoutée au cartes du joueur demandant.
     * @param carte Carte demandée.
     * @return True le joueur membre possède la carte.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    bool demanderCarte(const Carte& carte);

    /**
     * @brief Défini la prochaine carte à demander. Cette dernière est choisie aléatoirement entre les cartes non possédé de la famille la plus nombreuse.
     * @return La prochaine carte à demander.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    Carte carteADemander();

    /**
     * @brief Calcul la famille de laquelle le joueur a le plus de carte.
     * @return Le chiffre représentant la famille la plus nombreuse.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    unsigned familleLaPlusNombreuse();

    /**
     * @brief Compte le nombre de carte de chaque famille dans les cartes du joueur actuel.
     * @return Le nombre de carte de chaque famille.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    std::vector<int> compterCarteParFamille();

    /**
     * @brief Essaye de poser une famille complète. Si le joueur actuel possède toutes les cartes d'une famille, elles sont supprimées de ses cartes et il reçoit 1 point.
     * @return True si le joueur actuel avait une famille complète et a reçu 1 point.
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    bool detecterFamille();
};

#endif //LABO_03_JOUEUR_H
