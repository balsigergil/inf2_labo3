/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Carte.cpp
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Implémentation de la classe Carte.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#include "Carte.h"      // header
#include <iostream>     // cout 

Carte::Carte(char Membre, unsigned Famille) : membre(Membre), famille(Famille) {}

char Carte::getMembre() const {
    return membre;
}

unsigned Carte::getFamille() const {
    return famille;
}

bool operator==(const Carte& lhs, const Carte& rhs) {
    return lhs.membre == rhs.membre &&
           lhs.famille == rhs.famille;
}

bool operator!=(const Carte& lhs, const Carte& rhs) {
    return !(rhs == lhs);
}

std::ostream& operator<<(std::ostream& os, const Carte& carte) {
    os << carte.famille << carte.membre;
    return os;
}

