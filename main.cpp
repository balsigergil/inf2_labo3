/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : main.cpp
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : L'object de ce labo est de réaliser le jeu des 7 familles en C++.
               Pour cela, il est demandé d'implémenter une classe "Partie" permettant de
               contrôler le jeu et une classe "Joueur" permettant d'y jouer.
               Une classe "Carte" est également implémenté pour gérer les cartes dans une partie.

               Plusieurs instances de la classe joueur doivent pouvoir jouer au jeu mais il n'est
               pas demandé qu'un humain puisse jouer contre le système.

 Remarque(s) : Code et commentaires en français.
               Ce labo est réalisé par groupe de 3.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#include <iostream>
#include <cstdlib>
#include "Partie.h"
#include "Joueur.h"

using namespace std;

int main() {
    // initialisation de joueurs
    Joueur alice("Alice", true), bobby("Bobby"), carol("Carol"), danny("Danny");
    const vector<Joueur*> joueurs{
            &alice,
            &bobby,
            &carol,
            &danny
    };

    // vecteur pour l'enregistrement des scores
    // taille réservée aux nombres joueurs
    vector<int> score(joueurs.size());

    // initialisation d'une partie
    Partie partieUne(joueurs);
    score = partieUne.jeu();
    // affichage du score pour chaque joueur
    for (Joueur* j : joueurs) {
        cout << "le score du " << j->getNom() << " est de : " << j->getPoints() << endl;
    }

    return EXIT_SUCCESS;
}
