/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Carte.h
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Classe permettant la gestion des cartes dans une partie.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#ifndef LABO_03_CARTE_H
#define LABO_03_CARTE_H

#include <ostream>   // surcharge opérateur de flux

const char PREMIER_MEMBRE_CARTE = 'A';
const unsigned PREMIER_FAMILLE_CARTE = 1;

/**
 * @class Carte Gestion des données d'une carte (famille et membre)
 * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
 * @date 15.03.2019
 */
class Carte {
    /**
     * @brief surcharge de l'opérateur !=
     * @param lhs  valeur de gauche
     * @param rhs  valeur de droite
     * @return bool
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    friend bool operator!=(const Carte& lhs, const Carte& rhs);

    /**
     * @brief surcharge de l'opérateur ==
     * @param lhs  valeur de gauche
     * @param rhs  valeur de droite
     * @return bool
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    friend bool operator==(const Carte& lhs, const Carte& rhs);

    /**
     * @brief surcharge de l'opérateur de flux
     * @param flux dans lequel on envoie les données
     * @param Carte   classe carte à afficher
     * @return flux de sortie
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    friend std::ostream& operator<<(std::ostream& os, const Carte& carte);

public:
    /**
     * @brief constructeur de classe Carte
     * @param membre     indique la position de la carte dans la famille
     * @param Famille    indique la famille de la carte
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    Carte(char Membre, unsigned Famille);

    /**
     * @brief retourne la position de la carte dans la famille
     * @return retourne la position de la carte dans la famille
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    char getMembre() const;

    /**
     * @brief retourne la famille de la carte
     * @return retourne la famille de la carte
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    unsigned getFamille() const;

private:
    /**
     * @brief contient la position de la carte dans la famille
     */
    char membre;
    
    /**
     * @brief contient la famille de la carte
     */
    unsigned famille;
};

#endif //LABO_03_CARTE_H