/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Partie.h
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Classe permettant la gestion d'une partie de jeu.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#ifndef LABO_03_PARTIE_H
#define LABO_03_PARTIE_H

#include <vector>       // utilisation de vector
#include "Joueur.h"     // utilisation de la classe Joueur

/**
  * @class Gestion d'une partie
  * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
  * @date 15.03.2019
  */
class Partie {

public:
    /**
     * @brief constructeur de la classe partie
     * @param joueurs  liste de joueurs
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    Partie(const std::vector<Joueur*>& joueurs);

    /**
     * @brief fonction principale où se déroule la partie
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    std::vector<int> jeu();

private:
    std::vector<Joueur*> joueurs;
    std::vector<Carte> pioche;

    /**
     * @brief construction de la pioche puis mélange aléatoire
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    void initPioche();

    /**
     * @brief retourne le score de tous les joueurs
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    std::vector<int> scoreJoueurs();

    /**
     * @brief pioche une carte dans la pioche
     * @return retourne la carte piochée
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    Carte piocherCarte();

    /**
     * @brief distribue le nombre de carte par joueur pour le début jeu
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    void initJeuJoueur();

    /**
     * @brief compte le nombre de joueur(s) encore en jeu
     * @return bool    vrai s'il y a plus de un joueur
     * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
     */
    bool joueursEncoreEnJeu() const;
};

#endif //LABO_03_PARTIE_H
