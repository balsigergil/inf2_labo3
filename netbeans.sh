#!/bin/bash

echo ""
echo "Netbeans project creation script"
echo ""

export TMPDIR='temp' DISTDIR='dist' LAB_ID='Labo 03'

mkdir -p $TMPDIR

ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0`

tail -n+$ARCHIVE $0 | tar xzv -C $TMPDIR

echo "Copying sources to temp dir"
cp *.h $TMPDIR
cp *.cpp $TMPDIR
cd $TMPDIR

echo "Creating Netbeans project"
item_h=$(ls *.h | sed 's/\(.*\)/<itemPath>\1<\\\/itemPath>/' | tr '\r\n' ' ')
item_cpp=$(ls *.cpp | sed 's/\(.*\)/<itemPath>\1<\\\/itemPath>/' | tr '\r\n' ' ')
conf_h=$(ls *.h | sed 's/\(.*\)/<item path="\1" ex="false" tool="3" flavor2="0"><\\\/item>/' | tr '\r\n' ' ')
conf_cpp=$(ls *.cpp | sed 's/\(.*\)/<item path="\1" ex="false" tool="1" flavor2="0"><\\\/item>/' | tr '\r\n' ' ')
sed -i "s/###ITEM_H###/${item_h}/g" nbproject/configurations.xml
sed -i "s/###ITEM_CPP###/${item_cpp}/g" nbproject/configurations.xml
sed -i "s/###CONF_H###/${conf_h}/g" nbproject/configurations.xml
sed -i "s/###CONF_CPP###/${conf_cpp}/g" nbproject/configurations.xml
sed -i "s/###LAB_ID###/${conf_cpp}/g" nbproject/project.xml

mkdir -p ../$DISTDIR

if [ $1 = "--no-zip" ]
then
    cp -r * ../$DISTDIR
else
    echo "Creating zip archive"
    zip -r ../$DISTDIR/netbeans.zip *
fi

cd ..
rm -rf $TMPDIR

exit 0

__ARCHIVE_BELOW__
�      �Y�o�8��H�?X���AJi�(tC�Q��X��@��9�-:��~ώ�8�鷵���R!�����~~�|M��G޽�8�qrr"�a�[���F��<l7Z0�h�:xM����8f�cA��{h�{�=��k�"wn�QDn������ڵ���b����#�|�0uԲ�)Z$�6q�2` G�w��%��@օ����&�`��*&@P8D��KN�� �4�m�<0ꋍ�9�PL�" Z��K�}U�"�6��z�Ym�`��XI����;X�ł����XI��� �(��4{K����Y��(���&^h�Y)�̬e�y�yN�;T�.�ʥԥ+t��5گ��ԥG.�: ��"�>_�����c����ɮIs�q���� S�'�-��2���0�Bb�K�Fv@��jð؁�[����?�!�#qDh���02v�:���ơT�地�O���2�z���H�$^j/�~�}HvX�g�z�?��D�+bJ�X<�P$�]�,�Be�����u�ZE�w�]/<Q��	���oQ;�Q����߽��`4+�Cm�S!`[YJ��'���u�AF�sDR�yRę��+q�^��qw�m��+�m4�auԘ[��!q����z
xq~f<��9��1p�1ل>�qo~v1�\���@�/I p
xǏq�Qt�z���ן��fTf����S@�{�a����|�io�%�����x�^�k��`s���װ���76���@y+S3���`0�T�qHo\P�CQe�l=����Jzj��~�7���O�}��.�,ґ���O�JEk���8q�j-�8��	�<c�'�Vtrܰ���dm��϶� v�T��Q�@A��Њ^Y#�TF�Ɗ�n�{)!k8J�N����g�
�I5#B���_;�  ;iCJ�����H�y�e����1��Iģ8sj�9�|�g��ڙK1�����
"��.3 �U��O _����4���b��j
�@��3��8g+⣭�g���y%%�������eGwY	��¥���Iu�g�,7(9�+	]� L
�	-���	��s�˴x=��峸��4�����|�z��_O�l����,���~�{/%���Zq���Ã������~����	�n� ��F����	�����Y�c�S��J'������X�@�P�V.\��ρk�hO��}Uq��}{�M��;��*gRE�.uN����y�����"9�1Y�Ƣ����=�U�	����h>�\}������>Eg$�C�&�U5���OP��Ij]��R����s|8�ⱊ
�ǫ��^D̄q"·�����Q�7�:���g�zx�	�0�H#F~0��KZ2�L/f�����l4^��]N�)\ºIOұҩ��jI�!W$��/Q"C�(��b��"�ᥡ�ʃ��.	�f��"��犎�Oj�	�ov��������"a;�aAccD��S��5�+�t��;�a%U�*i��:�r&�s ��A��E̜�ǎ�>�( ̇��KI��]��V�NT�^�g�B�@F���Jʡ��
�S���,�칽���5RD����'��o�rXrC� �	�I��B��T.2c�<	��S�yBj��*ô�"��.���#��E1^l�ď�v����t������%5����_��j�Z��7O�����Sƣ���� ��j�9ۖu{{[��/�+c�V��D�Ң�E���8�;�M�����l���9~�"�V�&E'Qv����h KN��8B�"����r^a���
ky�	��<Y���C�����l�G��^₲�1�2{�F�l�_,�ӵ�G|�Z�\ٻ����x7rqϭ[�z���x5������c��q��*$����-�ª�ع^o'쐮~���(����m���������  (  