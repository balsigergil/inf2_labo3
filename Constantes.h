/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Constantes.h
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Fichier header fournissant les paramètres d'executions aux parties du jeu.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#ifndef LABO_03_CONSTANTES_H
#define LABO_03_CONSTANTES_H

// Donne initiale
const int CARTE_PAR_JOUEUR = 4;
const int NOMBRE_FAMILLES = 7;
const int CARTES_PAR_FAMILLE = 5;

const int NOMBRE_PARTIES = 100;

#endif //LABO_03_CONSTANTES_H
