/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Outil.cpp
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Implémentation de la classe Outil.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#include "Outil.h"   // header 
#include <cstdlib>   // librairie de base 
#include <ctime>     // pour avoir le temps 

int genererNombreAleatoire(int min, int max) {
    // A la première utilisation, génération du seed pour la fonction rand()
    static bool premiereExecution = true;
    if (premiereExecution) {
        srand((unsigned) time(NULL));
        premiereExecution = false;
    }

    // Génération d'un nombre aléatoire entre min et max
    return rand() % (max - min + 1) + min;
}

int genererNombreAleatoire(int max) {
    return genererNombreAleatoire(0, max);
}
