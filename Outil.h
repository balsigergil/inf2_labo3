/*
 -----------------------------------------------------------------------------------
 Laboratoire : Labo 3
 Fichier     : Outil.h
 Auteur(s)   : Jérôme Arn, Gil Balsiger, Julien Béguin
 Date        : 15.03.2019

 But         : Boite à outils fournissant des fonctions de générations aléatoire.

 Compilateur : MinGW-g++ 6.3.0
 -----------------------------------------------------------------------------------
 */

#ifndef LABO_03_OUTIL_H
#define LABO_03_OUTIL_H

/**
 * @brief Génère un nombre aléatoirement entre min (inclus) et max (inclus).
 * @param min int, Valeur minimale incluse.
 * @param max int, Valeur maximale incluse.
 * @return Nombre généré aléatoirement.
 * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
 */
int genererNombreAleatoire(int min, int max);

/**
 * @brief Génère un nombre aléatoirement entre 0 (inclus) et max (inclus).
 * @param max int, Valeur maximale incluse.
 * @return Nombre généré aléatoirement.
 * @authors Jérôme Arn, Gil Balsiger, Julien Béguin
 */
int genererNombreAleatoire(int max);

#endif //LABO_03_OUTIL_H
